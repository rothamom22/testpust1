import React, { Component } from 'react'

export class Counter extends Component {
    constructor(props) {
        super(props)
        this.state ={
            Counter:0
        }
    }
    handleIncrease = ()=> {
        this.setState({
            counter:this.state.counter +1
        })
    }
    handleDecrease = ()=>{
        this.setState({
            counter:this.state.counter -1
        })
    }
    componentDidMount() {
        console.log("Our component is mounted!!")
    }
    componentDidUpdate() {
        console.log("Our component is update!!")
    }
    componentWillUnmount() {
        console.log("Our component is unmount!!")
    }
    render() {
        return (
            <div className='text-center mt-4'>
                <h1>{this.state.Counter}</h1>
                <div className="" >
                    <button className= "btn btn-warning mx-2" 
                    onClick={this.handleIncrease} 
                    >(+)</button>
                    <button className= "btn btn-danger mx-2" 
                    onClick={this.handleDecrease} >(-)</button>
                </div>
            </div>
        )
    }
}

export default Counter
