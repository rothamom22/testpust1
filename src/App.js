import logo from './logo.svg';
import './App.css';
import Counter from './components/Counter';
import "bootstrap/dist/css/bootstrap.min.css"
import UserPage from './pages/UserPage';
import Homepage from './pages/Homepage';
import ServicePage from './pages/ServicePage';
import {Router, Routes, Route} from 'react-router-dom'
import MyNavBar from './pages/MyNavBar';
import NotFoundPage from './pages/NotFoundPage';

function App() {
  return (
    <div>
      <MyNavBar/>
       <Routes>
     <Route path= "/" element ={<Homepage/>} />
     <Route path= "/service" element ={<ServicePage/>} />
     <Route path= "/user" element ={<UserPage/>} />
     <Route path= "*" element ={<NotFoundPage/>} />
   </Routes>
    </div>
  
  )
}

export default App;
