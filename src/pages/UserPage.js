import React, { useState } from 'react'

const UserPage = () => {
    const [name, setName] = useState('Miss. Nita')
    const [gender, setGander] = useState('female')
    const [email, setEmail] = useState('nita@gmail.com')
    const [profileUrl, setProfileUrl] = useState("https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQpsFMhnFPMvn2_znLscyqfvaRQ_3xc7sJYT7wPKW0&s")

    const handleTransform = () => {
        setName("Mr.Jonh")
        setEmail("jonh@gmail.com")
        setGander("Male")
        setProfileUrl("https://i.pinimg.com/236x/5c/8e/1c/5c8e1c34754753fa4a431cb35703122f.jpg")
    }
    return (
        <div className ='container mt-5 px-4  py-5 bg-light rounded-3' >
          <div className='d-flex align-items-center'>
              <img className='img-fluid w-25 rounded-circle' src={profileUrl} alt="user proflie"/>
              <div className='ms-5' >
                  <h3>{name} </h3>
                  <p>{email} </p>
                  <p>{gender} </p>
                  <button className='btn btn-warning' 
                  onClick ={handleTransform}
                  >Transform</button>
              </div>
            </div> 
        </div>
    )
}

export default UserPage
