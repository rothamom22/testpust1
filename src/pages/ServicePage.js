import React from 'react'

function ServicePage() {
    return (
        <div>
            <h1>Service page</h1>
            <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quae ab odit facere rerum animi necessitatibus fuga magni voluptatem maiores recusandae, quas voluptates aliquam, aspernatur earum impedit nam explicabo, accusamus nobis.</p>
            <button className= "btn btn-success"> Explore More
                
            </button>
        </div>
    )
}

export default ServicePage
