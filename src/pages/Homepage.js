import React from 'react'

function Homepage() {
    return (
        <div className='container' >
            <h1> Home Page</h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aliquid tempora alias sapiente, doloribus rem incidunt saepe sequi! Obcaecati voluptas, fugit consectetur eos eum dolores, tempore aut debitis quas nesciunt recusandae!</p>
            <button 
            type = "button"
            class = "btn btn-warning">
            sign up</button>
        </div>
    )
}

export default Homepage
